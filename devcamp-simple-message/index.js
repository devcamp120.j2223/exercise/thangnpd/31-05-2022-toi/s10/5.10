const express = require('express');
const app = express();
const port = 8000;
const date = new Date().getDate();


app.get('/', (req, res) => {
  res.send(`Xin chào, hôm nay là ngày ${date}`);
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
})